package com.cookpad.taskassignment.Utils;

import com.cookpad.taskassignment.Models.Recipe;
import com.cookpad.taskassignment.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DineshSVNV on 29-10-17.
 */

public class GenericMethods {

    public static int[] getUsers(){
        //Listing user avatar
        int[] user_icon = new int[]{
                R.drawable.user1,
                R.drawable.user2,
                R.drawable.user3,
                R.drawable.user4,
                R.drawable.user1,
                R.drawable.user2,
                R.drawable.user3,
                R.drawable.user4};

        return user_icon;
    }

    public static int[] getRecipes(){
        // Listing recipes images
        int[] recipesImage = new int[]{
                R.drawable.image10,
                R.drawable.image20,
                R.drawable.image30,
                R.drawable.image40,
                R.drawable.image50,
                R.drawable.image60,
                R.drawable.image70,
                R.drawable.image80};

        return recipesImage;
    }

    public static List<Recipe> getListOfRecipes(){

        //Listing user avatar
        int[] user_icon = GenericMethods.getUsers();

        // Listing recipes images
        int[] recipesImage = GenericMethods.getRecipes();

        List<Recipe> RecipeList = new ArrayList<>();

        Recipe a = new Recipe(AppConstants.ITEM1, user_icon[0], "Jon Snow", "Today 14:30", "1314", recipesImage[0], "item1");
        RecipeList.add(a);

        a = new Recipe(AppConstants.ITEM2, user_icon[1], "Arya Stark", "Today 11:30", "2458", recipesImage[1], "item2");
        RecipeList.add(a);

        a = new Recipe(AppConstants.ITEM3, user_icon[2], "Robb Stark", "Yesterday 18:42", "121", recipesImage[2], "item3");
        RecipeList.add(a);

        a = new Recipe(AppConstants.ITEM4, user_icon[3], "Sansa Stark", "Yesterday 09:18", "58", recipesImage[3], "item4");
        RecipeList.add(a);

        a = new Recipe(AppConstants.ITEM5, user_icon[4], "Jon Snow", "Yesterday 01:56", "6247", recipesImage[4], "item5");
        RecipeList.add(a);

        a = new Recipe(AppConstants.ITEM6, user_icon[5], "Arya Stark", "24 Oct 10:24", "123", recipesImage[5], "item6");
        RecipeList.add(a);

        a = new Recipe(AppConstants.ITEM7, user_icon[6], "Robb Stark", "22 Oct 16:36", "1489", recipesImage[6], "item7");
        RecipeList.add(a);

        a = new Recipe(AppConstants.ITEM8, user_icon[7], "Sansa Stark", "21 Oct 17:57", "8426", recipesImage[7], "item8");
        RecipeList.add(a);

        return RecipeList;

    }
}

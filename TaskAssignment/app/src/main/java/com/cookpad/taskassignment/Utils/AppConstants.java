package com.cookpad.taskassignment.Utils;

/**
 * Created by DineshSVNV on 25-10-17.
 */

public final class AppConstants {

    /*
    * Constants to use throught the app
    * */
    public static final String USER_ICON = "user_icon";
    public static final String RECIPE_IMAGE = "recipe_image";
    public static final String USER_NAME = "user_name";
    public static final String POST_TIME = "post_time";
    public static final String COMMENTS = "comments";
    public static final String RECIPE_DETAIL = "recipe_detail";
    public static final String RECIPE_NAME = "recipe_name";
    public static final String ITEM1 = "Butter Chicken";
    public static final String ITEM2 = "Palak Paneer";
    public static final String ITEM3 = "Kaju Barfi";
    public static final String ITEM4 = "Fried Salmon";
    public static final String ITEM5 = "Chicken Biriyani";
    public static final String ITEM6 = "Masala Dosa";
    public static final String ITEM7 = "Laddu";
    public static final String ITEM8 = "Noodles";
}

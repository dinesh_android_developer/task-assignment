package com.cookpad.taskassignment.Models;

import java.util.HashMap;

/**
 * Created by DineshSVNV on 24-10-17.
 */

public class Recipe {

    private String recipeName, userName, post_time, comments, receipe_detail;
    private int thumbnail, user_icon;

    public Recipe(String RecipeName, int userIcon,
                  String userName, String post_time,
                  String comments, int thumbnail, String recipeDetail) {
        /*
        * Setting Setters
        * */
        setRecipeName(RecipeName);
        setUser_icon(userIcon);
        setUserName(userName);
        setPost_time(post_time);
        setComments(comments);
        setThumbnail(thumbnail);
        setReceipe_detail(recipeDetail);
    }

    /*
    * RECIPE NAME
    * */
    public String getRecipeName() {
        return recipeName;
    }
    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }


    /*
   * USER NAME
   * */
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }


    /*
   * POST TIME
   * */
    public String getPost_time() {
        return post_time;
    }
    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    /*
   * RECIPE DETAIL TEXT
   * */
    public String getReceipe_detail() {
        return receipe_detail;
    }
    public void setReceipe_detail(String receipe_detail) {
        this.receipe_detail = receipe_detail;
    }


    /*
       * NUMBER OF COMMENTS
       * */
    public String getComments() {
        return comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }

    /*
   * USER ICON RESOURCE ID NUMBER
   * */
    public int getUser_icon() {
        return user_icon;
    }
    public void setUser_icon(int user_icon) {
        this.user_icon = user_icon;
    }

    /*
   * RECIPE IMAGE RESOURCE ID NUMBER
   * */
    public int getThumbnail() {
        return thumbnail;
    }
    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}

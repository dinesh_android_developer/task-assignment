package com.cookpad.taskassignment.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cookpad.taskassignment.Models.Recipe;
import com.cookpad.taskassignment.R;
import com.cookpad.taskassignment.Activities.RecipeActivity;
import com.cookpad.taskassignment.Utils.AppConstants;

import java.util.List;

/**
 * Created by DineshSVNV on 24-10-17.
 */

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.MyHolder> {

    private Context mContext;
    private List<Recipe> RecipeList;

    /*
    * Recipe Adapter Constructor
    * */

    public RecipeAdapter(Context context, List<Recipe> recipes) {
        this.mContext = context;
        this.RecipeList = recipes;
    }

    /*
    * Recipe Adapter inflate the layout
    * */

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recipe_cardview, parent, false);
        return new MyHolder(itemView);
    }

    /*
    * Recipe Adapter Bind all the view with the holder - MyHolder
    * */
    @Override
    public void onBindViewHolder(final RecipeAdapter.MyHolder holder, final int position) {

        final Recipe recipe = RecipeList.get(position);

        /*Set elements */
        holder.username.setText(recipe.getUserName());
        holder.post_time.setText(recipe.getPost_time());
        holder.comments.setText(recipe.getComments());
        holder.recipe_detail.setText(mContext.getResources().getString(mContext.getResources().getIdentifier(recipe.getReceipe_detail(), "string", mContext.getPackageName())));
        holder.recipe_detail_text = mContext.getResources().getString(mContext.getResources().getIdentifier(recipe.getReceipe_detail(), "string", mContext.getPackageName()));

        /*Holder values for passing in the intent*/
        holder.recipeName = recipe.getRecipeName();
        holder.user_icon_number = recipe.getUser_icon();
        holder.recipe_image = recipe.getThumbnail();
        holder.user_name = recipe.getUserName();
        holder.post_dated = recipe.getPost_time();
        holder.numOfComments = recipe.getComments();

        // loading images cover using Glide library
        //loading user avatar
        Glide.with(mContext).load(recipe.getUser_icon()).into(holder.user_icon);

        //loading recipe image
        Glide.with(mContext).load(recipe.getThumbnail()).into(holder.thumbnail);

        // apply shared element transition when the recipe image is clicked
        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Initialise intent
                Intent callIntent = new Intent(mContext, RecipeActivity.class);
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                //Pass data through intent
                callIntent.putExtra(AppConstants.RECIPE_NAME, holder.recipeName);
                callIntent.putExtra(AppConstants.USER_ICON, holder.user_icon_number);
                callIntent.putExtra(AppConstants.USER_NAME, holder.user_name);
                callIntent.putExtra(AppConstants.RECIPE_IMAGE, holder.recipe_image);
                callIntent.putExtra(AppConstants.RECIPE_DETAIL, holder.recipe_detail_text);
                callIntent.putExtra(AppConstants.POST_TIME, holder.post_dated);
                callIntent.putExtra(AppConstants.COMMENTS, holder.numOfComments);

                /*
                * If the build version is lollipop, apply shared element transistion
                * else just invoke Recipe Activity
                * */

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) mContext,
                            new Pair<View, String>(holder.thumbnail,
                                    AppConstants.RECIPE_IMAGE));

                    ActivityCompat.startActivity(mContext, callIntent, options.toBundle());
                } else {
                    mContext.startActivity(callIntent);
                }
            }
        });

        // apply shared element transition when the card view is clicked
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Initialize intent
                Intent callIntent = new Intent(mContext, RecipeActivity.class);
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                //Pass data through intent
                callIntent.putExtra(AppConstants.RECIPE_NAME, holder.recipeName);
                callIntent.putExtra(AppConstants.USER_ICON, holder.user_icon_number);
                callIntent.putExtra(AppConstants.USER_NAME, holder.user_name);
                callIntent.putExtra(AppConstants.RECIPE_IMAGE, holder.recipe_image);
                callIntent.putExtra(AppConstants.RECIPE_DETAIL, holder.recipe_detail_text);
                callIntent.putExtra(AppConstants.POST_TIME, holder.post_dated);
                callIntent.putExtra(AppConstants.COMMENTS, holder.numOfComments);

                 /*
                * If the build version is lollipop, apply shared element transistion
                * else just invoke Recipe Activity
                * */

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) mContext,
                            new Pair<View, String>(holder.thumbnail,
                                    AppConstants.RECIPE_IMAGE));

                    ActivityCompat.startActivity(mContext, callIntent, options.toBundle());
                } else {
                    mContext.startActivity(callIntent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return RecipeList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        /*
        * Defining Global variables
        * */
        public String recipeName, recipe_detail_text, user_name, post_dated, numOfComments;
        public int user_icon_number, recipe_image;
        public TextView recipe_detail, username, post_time, comments;
        public ImageView user_icon, thumbnail;
        public CardView cardView;

        public MyHolder(View itemView) {
            super(itemView);

         /*
        * initializing the variables
        * */
            recipe_detail = (TextView) itemView.findViewById(R.id.recipe_detail);
            post_time = (TextView) itemView.findViewById(R.id.post_time);
            comments = (TextView) itemView.findViewById(R.id.number_of_comments);
            username = (TextView) itemView.findViewById(R.id.Username);
            user_icon = (ImageView) itemView.findViewById(R.id.user_icon);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }
}

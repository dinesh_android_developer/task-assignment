package com.cookpad.taskassignment.Activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cookpad.taskassignment.R;
import com.cookpad.taskassignment.Utils.AppConstants;

public class RecipeActivity extends AppCompatActivity {

    private int user_icon, recipe_image_number;
    private ImageView recipeImage, User_Icon;
    private boolean LIKE_RECIPE = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        Toolbar toolbar = (Toolbar) findViewById(R.id.fragment_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getWindow().setAllowEnterTransitionOverlap(false);
        getWindow().setAllowReturnTransitionOverlap(false);

        String recipeName = getIntent().getStringExtra(AppConstants.RECIPE_NAME).toString();
        String recipeDetails = getIntent().getStringExtra(AppConstants.RECIPE_DETAIL).toString();
        String user_name = getIntent().getStringExtra(AppConstants.USER_NAME).toString();
        String post_dated = getIntent().getStringExtra(AppConstants.POST_TIME).toString();
        String numOfComments = getIntent().getStringExtra(AppConstants.POST_TIME).toString();
        user_icon = getIntent().getIntExtra(AppConstants.USER_ICON, 0);
        recipe_image_number = getIntent().getIntExtra(AppConstants.RECIPE_IMAGE, 0);


        recipeImage = (ImageView) findViewById(R.id.recipe_image_fragment);
        User_Icon = (ImageView) findViewById(R.id.user_icon);
        TextView Username = (TextView) findViewById(R.id.Username);
        TextView post_time = (TextView) findViewById(R.id.post_time);
        TextView comments = (TextView) findViewById(R.id.number_of_comments);
        TextView recipeDetail = (TextView) findViewById(R.id.recipe_detail_fragment);
        final FloatingActionButton like_recipe = (FloatingActionButton) findViewById(R.id.like_recipe);

        // BEGIN_INCLUDE(detail_set_view_name)
        /**
         * Set the name of the view's which will be transition to, using the static values above.
         * This could be done in the layout XML, but exposing it via static variables allows easy
         * querying from other Activities
         */
        ViewCompat.setTransitionName(recipeImage, AppConstants.RECIPE_IMAGE);
        // END_INCLUDE(detail_set_view_name)


        try {
            Username.setText(user_name);
            post_time.setText(post_dated);
            comments.setText(numOfComments);
            recipeDetail.setText(recipeDetails);

//            loadItem();

            //setting the bitmap from the drawable folder
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), recipe_image_number);
            Bitmap bitmap_user = BitmapFactory.decodeResource(getResources(), user_icon);

            //set the image to the imageView
            recipeImage.setImageBitmap(bitmap);
            User_Icon.setImageBitmap(bitmap_user);


        } catch (Exception e) {
            e.printStackTrace();
        }

        recipeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickHome();
            }
        });


        like_recipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (LIKE_RECIPE) {
                    like_recipe.setImageResource(R.drawable.ic_favorite_border_black_18dp);
                    LIKE_RECIPE = false;
                } else {
                    like_recipe.setImageResource(R.drawable.ic_favorite_black_18dp);
                    LIKE_RECIPE = true;
                }

            }
        });


    }


    private void loadItem() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && addTransitionListener()) {
            // If we're running on Lollipop and we have added a listener to the shared element
            // transition, load the thumbnail. The listener will load the full-size image when
            // the transition is complete.
            loadThumbnail();
        } else {
            // If all other cases we should just load the full-size image now
            loadFullSizeImage();
        }
    }

    /**
     * Load the item's thumbnail image into our {@link ImageView}.
     */
    private void loadThumbnail() {


        Glide.with(this)
                .load(recipe_image_number)
                .listener(new RequestListener<Integer, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, Integer model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, Integer model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        RecipeActivity.this.supportStartPostponedEnterTransition();
                        return false;
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(recipeImage);

        Glide.with(User_Icon.getContext())
                .load(user_icon)
                .dontAnimate()
                .dontTransform()
                .into(User_Icon);
    }

    private void loadFullSizeImage() {

        Glide.with(this)
                .load(recipe_image_number)
                .listener(new RequestListener<Integer, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, Integer model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, Integer model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        RecipeActivity.this.supportStartPostponedEnterTransition();
                        return false;
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(recipeImage);

        Glide.with(User_Icon.getContext())
                .load(user_icon)
                .dontAnimate()
                .dontTransform()
                .into(User_Icon);
    }


    /**
     * Try and add a {@link Transition.TransitionListener} to the entering shared element
     * {@link Transition}. We do this so that we can load the full-size image after the transition
     * has completed.
     *
     * @return true if we were successful in adding a listener to the enter transition
     */
    private boolean addTransitionListener() {
        final Transition transition = getWindow().getSharedElementEnterTransition();

        if (transition != null) {
            // There is an entering shared element transition so add a listener to it
            transition.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionEnd(Transition transition) {
                    // As the transition has ended, we can now load the full-size image
                    loadFullSizeImage();

                    // Make sure we remove ourselves as a listener
                    transition.removeListener(this);
                }

                @Override
                public void onTransitionStart(Transition transition) {
                    // No-op
                }

                @Override
                public void onTransitionCancel(Transition transition) {
                    // Make sure we remove ourselves as a listener
                    transition.removeListener(this);
                }

                @Override
                public void onTransitionPause(Transition transition) {
                    // No-op
                }

                @Override
                public void onTransitionResume(Transition transition) {
                    // No-op
                }
            });
            return true;
        }

        // If we reach here then we have not added a listener
        return false;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onClickHome();
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    protected void onClickHome() {
        super.onBackPressed();
    }
}

package com.cookpad.taskassignment.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.cookpad.taskassignment.Adapters.RecipeAdapter;
import com.cookpad.taskassignment.Models.Recipe;
import com.cookpad.taskassignment.R;
import com.cookpad.taskassignment.Utils.AppConstants;
import com.cookpad.taskassignment.Utils.GenericMethods;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Global variables
    private RecyclerView recyclerView;
    private RecipeAdapter adapter;
    private List<Recipe> RecipeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Setting up Toolbar & Toolbar title
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);

        //setting Title text here
        mTitle.setText("Indian Recipes");

        //To disable auto from toolbar theme
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //initializing RecyclerView
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_list_main);

        //initializing Recipe List
        RecipeList = new ArrayList<>();

        //initializing RecyclerView - Adapter
        adapter = new RecipeAdapter(this, GenericMethods.getListOfRecipes());

        //initializing Linear Layout Manager
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        //setting Linear Layout Manager to RecyclerView
        recyclerView.setLayoutManager(layoutManager);

        //Add an animation while show list of recipes
        int resId = R.anim.fall_down_animation;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_fall_down_animation);
        recyclerView.setLayoutAnimation(animation);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(adapter);

        // setting recipes list to the recyclerview
        preparingRecipes();

        //Initialing Drawer layout for navigation
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //setting up navigation drawer
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            recyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                }
            });
        }
    }


    /**
     * Adding few recipes to the list
     */
    private void preparingRecipes() {

        //Log.w("Recipe List: ", ""+RecipeList);
        //Notifying recyclerview adapter
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
